import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

const CACHE_KEY = 'httpRepoCache';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  repos: any;
  
  title = 'subjects_challenge';

  constructor(http: HttpClient) {
    const path = 'https://api.github.com/search/repositories?q=angular';
    this.repos = http.get<any>(path)
    .pipe(
      map(data => data.items)
      );

    this.repos.subscribe((next: any) => {
      //localStorage[CACHE_KEY].append(JSON.stringify(next));
      localStorage[CACHE_KEY] = JSON.stringify(next);
    });

    this.repos = this.repos.pipe(
      startWith(JSON.parse(localStorage[CACHE_KEY] || '[]'))
    )
  }

  public ngOnInit(): void {
    console.log(this.repos);
    console.log(localStorage.getItem(CACHE_KEY));
    console.log(JSON.parse(localStorage[CACHE_KEY]));
    const subject = new Subject<number>();

    subject.subscribe({
      next: (n) => console.log(`ObsA: ${n}`)
    });

    subject.subscribe({
      next: (n) => console.log(`ObsB: ${n + 1}`)
    });

    subject.next(1);
    subject.next(2);      
  }
}
